# test-drupal-ckeditor5-plugins

## Problem/Motivation
While trying to create a ckeditor 5 plugin for activitypub microsyntax I found that its plugin it cannot be loaded. I have reduced the code of the plugin to minimal and there's no way to load it.
After further testing apparently the reason is its name. In my tests every plugin that has a name that is first in alphabetical order than "ckeditor" cannot be loaded.

To check it out I have created three modules named `ckeditoq_plugin`, `ckeditos_plugin` and `ckeditot_plugin`. The only ones that could be loaded are `ckeditos_plugin` and `ckeditot_plugin`.The `ckeditoq_plugin` gives an error in browser dev tools console:

`Uncaught TypeError: i(...) is not a function        demoPluginQ.js:1:341`

This makes impossible to create a ckeditor5 plugin for activitypub module as plugin must have the module machine name as prefix in its id defined in `MODULE.ckeditor5.yml`


## Steps to reproduce
Create a simple plugin based on ckeditor5 plugin template in a module named as something before word ckeditor in the alphabetical order.
The three modules created to check this issue are in this repository.


node_modules are removed, to reinstall run `yarn install` in each module.
